<?php

return [
    'mongodb' => [
        'class' => '\yii\mongodb\Connection',
        'dsn' => 'mongodb://localhost:27001,localhost:27002,localhost:27003/booking',
    ],
];
