<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\HotelsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Hotels';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hotels-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Hotels', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            '_id',
            'name',
            'city',
            [
                'attribute' => 'stars',
                'format' => 'html',
                'value' => function($dataProvider) {
                    $string = '';

                    for ($i = 0; $i < $dataProvider->stars; $i++) {
                        $string .= '<span class="glyphicon glyphicon-star text-danger"></span>';
                    }

                    return $string;
                },
            ],
            'address',
            'phone',
            // 'email',
            // 'cost1',
            // 'cost2',
            // 'cost3',
            // 'cost4',
            // 'costLux',
            // 'freeCancel',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
