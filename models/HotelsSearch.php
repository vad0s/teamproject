<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Hotels;

/**
 * HotelsSearch represents the model behind the search form about `app\models\Hotels`.
 */
class HotelsSearch extends Hotels
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['_id', 'name', 'city', 'stars', 'address', 'phone', 'email', 'cost1', 'cost2', 'cost3', 'cost4', 'costLux', 'freeCancel'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Hotels::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere(['like', '_id', $this->_id])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'stars', $this->stars])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'cost1', $this->cost1])
            ->andFilterWhere(['like', 'cost2', $this->cost2])
            ->andFilterWhere(['like', 'cost3', $this->cost3])
            ->andFilterWhere(['like', 'cost4', $this->cost4])
            ->andFilterWhere(['like', 'costLux', $this->costLux])
            ->andFilterWhere(['like', 'freeCancel', $this->freeCancel]);

        return $dataProvider;
    }
}
