<?php

namespace app\models;

use Yii;

/**
 * This is the model class for collection "tickets".
 *
 * @property \MongoId|string $_id
 * @property mixed $from
 * @property mixed $to
 * @property mixed $date
 * @property mixed $cost
 */
class Tickets extends \yii\mongodb\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function collectionName()
    {
        return 'tickets';
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            '_id',
            'from',
            'to',
            'date',
            'cost',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['from', 'to', 'date', 'cost'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            '_id' => 'ID',
            'from' => 'From',
            'to' => 'To',
            'date' => 'Date',
            'cost' => 'Cost',
        ];
    }
}
