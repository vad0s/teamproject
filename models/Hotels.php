<?php

namespace app\models;

use Yii;

/**
 * This is the model class for collection "Hotels".
 *
 * @property \MongoId|string $_id
 * @property mixed $name
 * @property mixed $city
 * @property mixed $stars
 * @property mixed $address
 * @property mixed $phone
 * @property mixed $email
 * @property mixed $cost1
 * @property mixed $cost2
 * @property mixed $cost3
 * @property mixed $cost4
 * @property mixed $costLux
 * @property mixed $freeCancel
 */
class Hotels extends \yii\mongodb\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function collectionName()
    {
        return ['booking', 'Hotels'];
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            '_id',
            'name',
            'city',
            'stars',
            'address',
            'phone',
            'email',
            'cost1',
            'cost2',
            'cost3',
            'cost4',
            'costLux',
            'freeCancel',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'city', 'stars', 'address', 'phone', 'email', 'cost1', 'cost2', 'cost3', 'cost4', 'costLux', 'freeCancel'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            '_id' => 'ID',
            'name' => 'Назва',
            'city' => 'Місто',
            'stars' => 'Клас',
            'address' => 'Адреса',
            'phone' => 'Телефон',
            'email' => 'Email',
            'cost1' => '1-місний номер',
            'cost2' => '2-місний номер',
            'cost3' => '3-місний номер',
            'cost4' => '4-місний номер',
            'costLux' => 'Номер Люкс',
            'freeCancel' => 'Безкоштовна відміна за',
        ];
    }
}
